from optparse import OptionParser
import sys

from redux import cycle
from redux.detectors import (
    ClassFailureDetector,
    MessageFailureDetector,
)
from redux.exc import (
    CantFindTargetFunctionError,
    UnexpectedSuccessError,
)
from redux.utils import to_source


def error(msg):
    sys.stderr.write("Error: %s\n" % msg)


def main():

    parser = OptionParser("usage: %prog [options] file")
    parser.add_option("-f", "--function", dest="function",
                      help="function to call to trigger the bug")
    parser.add_option("-e", "--exception", dest="exception",
                      help="name of the exception raised by the bug")
    parser.add_option("-m", "--message", dest="message",
                      help="message of the exception raised by the bug")
    (options, args) = parser.parse_args()

    if len(args) < 1:
        parser.error("missing filename")

    filename = args[0]

    if options.exception and options.message:
        parser.error("exception and message are mutually exclusive")

    detector = None
    if options.exception:
        detector = ClassFailureDetector(options.exception)
    elif options.message:
        detector = MessageFailureDetector(options.message)

    try:
        result = cycle(filename, options.function, detector)
        print to_source(result)
    except (CantFindTargetFunctionError, UnexpectedSuccessError) as e:
        error(e)
        error(e.advice)


if __name__ == "__main__":
    main()
