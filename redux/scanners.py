import ast

from .utils import node_targets


class Scanner(ast.NodeVisitor):

    def visit(self, node):
        for child in ast.iter_child_nodes(node):
            child._parent = node
        self.generic_visit(node)

        self.scan(node)

    @property
    def name(self):
        return self.__class__.__name__.lower()


class FindUnusedAssignments(Scanner):
    """
    Find assignment nodes which assign targets that are not used
    later in the parent.

    Stores whole assign/for nodes that are unused in .unused
    Stores a list (node, child node) if only some child nodes
    are unused from the node
    """
    def __init__(self):
        self.unused = []
        self.partial_unused = {}

    def find_usages(self, node, target_node, after_line):
        # Name is terminal
        if node.__class__ is ast.Name:
            # the current node is using the target, and under the original,
            # report the usage
            if node.lineno > after_line and node.id == target_node.id:
                return [node]
            return []

        # find usages in child nodes
        result = []
        for child in ast.iter_child_nodes(node):
            result += self.find_usages(child, target_node, after_line)
        return result

    def scan(self, node):
        if node.__class__ not in [ast.Assign, ast.For]:
            return

        completely_unused = True
        partials = []
        for target_node in node_targets(node):

            usages = self.find_usages(node._parent, target_node, after_line=node.lineno)

            if len(usages) > 0:
                completely_unused = False
            else:
                partials.append(target_node)

        if completely_unused:
            self.unused.append(node)
        elif len(partials):
            self.partial_unused[node] = partials
