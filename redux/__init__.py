import ast
import copy
import shutil
import os

from .filters import (
    RemoveDeleted,
    RemoveUnusedDeclarations,
    RemoveUnusedLines,
    RemoveExtraPasses,
    RemoveEmptyIfs,
    RemoveFunctionCall,
    ReplaceFunctionCallByReturnValue,
    SimplifyFormats,
    SimplifyRemovePrints,
    RemovePrints,
    RemoveAsserts,
    RemoveSpecificNodes,
)
from .exc import UnexpectedSuccessError
from .scanners import (
    FindUnusedAssignments,
)
from .utils import run, load_source


# TODO: remove unused classes (check meta)
# TODO: look into decorators side effects
# TODO: remove unused imports after cleanup
# TODO: support multiple files/modules
# TODO: unit test for the filters
# TODO: use coverage over multiple runs before cleaning

def prepare_workspace():
    cwd = os.path.abspath(".")
    working_dir = os.path.join(cwd, "workspace")
    if os.path.exists(working_dir):
        shutil.rmtree(working_dir)
    os.makedirs(working_dir)
    return working_dir


def list_hypotheses(tr, current_ast):

    scanner = FindUnusedAssignments()
    scanner.visit(current_ast)

    remove_assignments_hypotheses = []

    for unused in scanner.unused:
        remove_assignments_hypotheses.append(
            RemoveSpecificNodes(unused)
        )

    basic_hypotheses = [
        RemovePrints(),
        RemoveAsserts(),
    ]
    remove_fn_hypotheses = []
    replace_fn_hypotheses = []

    for (filename, line_no, func_name), values in tr.results.items():
        for index, val in enumerate(values):
            replace_fn_hypotheses.append(
                ReplaceFunctionCallByReturnValue(filename, line_no,
                                                 func_name, index, val))
            remove_fn_hypotheses.append(
                RemoveFunctionCall(filename, line_no, func_name, index)
            )

    # return hypotheses in the order of maximum reduction
    return (basic_hypotheses + remove_fn_hypotheses +
            replace_fn_hypotheses + remove_assignments_hypotheses)


def run_hypotheses(hypotheses, filename, start_ast, function_name=None,
                   error_detector=None):
    best_ast = ast.fix_missing_locations(start_ast)
    best_tr = None
    modified = False

    for hypothesis in hypotheses:
        local_ast = copy.deepcopy(best_ast)
        modified_ast = hypothesis.visit(local_ast)
        if hypothesis.modified_ast():
            tr, ns, raised = run(filename, modified_ast, function_name,
                                 error_detector)

            if raised:
                modified = True
                best_ast = modified_ast
                best_tr = tr

    return modified, best_ast, best_tr


def apply_safe_transforms(start_ast, tr, filename):
    chain = [
        RemoveUnusedDeclarations(run=tr.coverage[filename],
                                 node_type=["class", "function"]),
        RemoveUnusedLines(run=tr.coverage[filename]),
        SimplifyFormats(),
        RemoveExtraPasses(),
        RemoveEmptyIfs(),
        SimplifyRemovePrints(),
        RemoveDeleted(),
    ]
    current_ast = start_ast
    modified = False
    for ast_filter in chain:
        current_ast = ast_filter.visit(current_ast)
        modified = modified or ast_filter.modified_ast()
    return modified, current_ast


def cycle(filename, function_name=None, error_detector=None):
    tr, _, raised = run(filename, None, function_name, error_detector)
    if not raised:
        raise UnexpectedSuccessError(filename, None)

    current_ast = ast.parse(load_source(filename), filename=filename)

    count = 1
    while True:
        modified, current_ast = apply_safe_transforms(current_ast,
                                                      tr, filename)
        tr, _, raised = run(filename, current_ast, function_name,
                            error_detector)
        if not raised:
            raise UnexpectedSuccessError(filename, current_ast)

        hypotheses = list_hypotheses(tr, current_ast)
        modified, current_ast, tr = run_hypotheses(
            hypotheses, filename, current_ast, function_name, error_detector)

        if not modified:
            break
        count += 1

    return current_ast
