import unittest

from .base import BaseTest

from redux import cycle
from redux.detectors import (
    ClassFailureDetector,
    MessageFailureDetector,
)
from redux.exc import (
    UnexpectedSuccessError,
    CantFindTargetFunctionError,
)


class CycleTest(BaseTest, unittest.TestCase):

    sources = [
        ("source1.py", "redux_start", None),
        ("source2.py", None, ClassFailureDetector(ZeroDivisionError)),
        ("source2.py", None, ClassFailureDetector("ZeroDivisionError")),
        ("source2.py", None, MessageFailureDetector(".* division .* by zero")),
        ("source3.py", "trigger", ClassFailureDetector(ZeroDivisionError)),
    ]

    def test_cant_find_function(self):
        invalid_fn_name = "__doesnt_exist__"
        for source, _, _ in self.sources:
            with self.assertRaises(CantFindTargetFunctionError) as cm:
                cycle(self.path(source, invalid_fn_name))
            self.assertTrue(cm.exception.filename, source)
            self.assertTrue(cm.exception.function_name, invalid_fn_name)

    def test_cant_find_function(self):
        for source, fn_name, _ in self.sources:
            if not fn_name:
                continue
            # these test files won't fail without the function call
            with self.assertRaises(UnexpectedSuccessError) as cm:
                cycle(self.path(source))
            self.assertTrue(cm.exception.filename, source)

    def test_compare(self):
        for source, fn_name, detector in self.sources:
            # these test files won't fail without the function call
            result = cycle(self.path(source), fn_name, detector)
            self.verify_result(result, source, "cycle", ignore_lines=set([2]))

