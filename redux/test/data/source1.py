from redux.exc import SentinelException

from source1module.a import function3


def function1(a):
    pass


def default_value(a=1):
    pass


def default_value(a=function1(1)):
    pass


def varargs(*args):
    pass


def kwargs(**kwargs):
    print kwargs
    pass


def function2():
    return [1, 2, 3]


def function3():
    a = "aaa"
    return a


def buggy_func(a):
    function3()
    z = 7
    k = None
    i = z + 3
    print "before", function2(), function2()
    print "format %s" % 1
    print function3()
    print u"test"
    yy = function3()
    assert function3()
    if a > 2:
        if expensive_call(a):
            print "ugh"
        return 1
    print "after"
    raise NotImplementedError()


def redux_start():
    print "a"
    try:
        buggy_func(1)
    except NotImplementedError:
        print "sentinel"
        raise SentinelException()

print "zz"
