
# called by default in fn3
def fn1():
    x = 1
    print x
    return x


# called in fn4
def fn2():
    x = 2
    print x
    return x


def fn3(a=fn1()):
    x = 3
    print x
    return x


def fn4():
    fn2()


def fn5():
    pass


def fn6():
    print 1


class B(object):

    def fn1(self):
        x = 1
        print x
        return x
