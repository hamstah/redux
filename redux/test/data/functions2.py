def fn1():
    pass

def fn2():
    pass

# TODO: filter should keepthe call to fn1
def fn3(a=fn1()):
    pass

def fn4():
    fn2()

def fn5():
    pass

class A(object):
    pass

class B(object):

    def fn1(self):
        pass

# TODO: add class with meta
