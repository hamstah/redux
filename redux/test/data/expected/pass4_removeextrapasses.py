import sys
sys.path.insert(0, 'redux/test')
from redux.exc import SentinelException
from source1module.a import function3

def buggy_func(a):
    raise NotImplementedError()

def redux_start():
    try:
        buggy_func(1)
    except NotImplementedError:
        raise SentinelException()