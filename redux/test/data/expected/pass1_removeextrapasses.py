def fn1():
    pass

def fn2():
    pass

def fn3():
    a = 1

def fn4():
    a = 1

def fn5():
    a = 1


class Class1(object):
    pass


class Class2(object):
    pass


class Class3(object):

    def fn1(self):
        pass


class Class4(object):

    def fn1(self):
        pass


class Class5(object):

    def fn1(self):
        pass
