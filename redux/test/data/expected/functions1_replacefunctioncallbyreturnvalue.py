def nonefn():
    return None

def intfn():
    return 1

def idfn(a):
    return a

def lfn():
    return [1, None, '']
None
x = None
y = (None or 12)
print None
print ('test: %s' % None)
print 'test:', None
z = (None, None)
1
x = 1
y = (1 + 1)
print 1
print ('test: %d' % 1)
print 'test:', 1
z = (1, 1)
12
x = 34
y = (56 + 1)
print 78
print ('test: %d' % 90)
print 'test:', 12
z = (1, 2)
lfn()
x = lfn()
y = (lfn() or 12)
print lfn()
print ('test: %s' % lfn())
print 'test:', lfn()
z = (lfn(), lfn())
k = (1, None, 2, lfn())
if None:
    print 'not ok'
if (1 == 1):
    print 'ok'
if len(lfn()):
    print 'ok'
