def fn1():
    x = 1
    return x

def fn2():
    x = 2
    return x

def fn3(a=fn1()):
    x = 3
    return x

def fn4():
    fn2()

def fn5():
    pass

def fn6():
    pass


class B(object):

    def fn1(self):
        x = 1
        return x
