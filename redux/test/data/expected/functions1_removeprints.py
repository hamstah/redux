def nonefn():
    return None

def intfn():
    return 1

def idfn(a):
    return a

def lfn():
    return [1, None, '']
nonefn()
x = nonefn()
y = (nonefn() or 12)
z = (nonefn(), nonefn())
intfn()
x = intfn()
y = (intfn() + 1)
z = (intfn(), intfn())
idfn(12)
x = idfn(34)
y = (idfn(56) + 1)
z = (idfn(1), idfn(2))
lfn()
x = lfn()
y = (lfn() or 12)
z = (lfn(), lfn())
k = (intfn(), nonefn(), idfn(2), lfn())
if nonefn():
    pass
if (intfn() == 1):
    pass
if len(lfn()):
    pass
