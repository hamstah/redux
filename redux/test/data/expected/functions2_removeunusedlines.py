def fn1():
    pass

def fn2():
    pass

def fn3(a=fn1()):
    pass

def fn4():
    pass

def fn5():
    pass


class A(object):
    pass


class B(object):

    def fn1(self):
        pass
