# print of basic types (numbers, bool, None, string)
print 'aaa'
print 'aaa', 'bbb'
print 1
print 'aaa', 1
print True, False, None, 1.2
print u"test"

# print of variable and basic_type
a = 5
print a, 1

# formats
print 'test: %s' % 'aaa'
print 'test: %s' % 1
print 'test: %s' % True
print 'test: %s' % None
a = 5
print 'test: %d' % a
print 'test: %d %d' % (1, 2)
print 'test: %d %d' % (1, a)

# concat
print "test" + "test2"

# product
print "test" * 3
a = 5
print "test" * a
