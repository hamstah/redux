def fn1():
    return 0


def fn2(x):
    return x / fn1()


def trigger():
    fn2(1)
