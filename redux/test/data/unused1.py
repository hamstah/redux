def fn1():
    a = 5


def fn2(b):
    b = 3


def fn3():
    for i in xrange(10):
        pass


def fn4():
    a = 4
    b = a + 4


def fn5():
    a, b = 1, 2


def fn6():
    a = 4
    b = 5
    b, a = a, b


def fn7():
    for index, i in enumerate(xrange(10)):
        pass


def fn8():
    for i in xrange(10):
        print i


def fn9():
    for index, i in enumerate(xrange(10)):
        print index


def fn10():
    for index, i in enumerate(xrange(10)):
        print index, i


def fn11():
    a, b = 1, 2
    print b


def fn12():
    for i, j, k in (xrange(10), xrange(10), xrange(10)):
        print j

