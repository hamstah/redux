def nonefn():
    return None

def intfn():
    return 1

def idfn(a):
    return a

def lfn():
    return [1, None, ""]

nonefn()
x = nonefn()
y = nonefn() or 12
print nonefn()
print "test: %s" % nonefn()
print "test:", nonefn()
z = nonefn(), nonefn()

intfn()
x = intfn()
y = intfn() + 1
print intfn()
print "test: %d" % intfn()
print "test:", intfn()
z = intfn(), intfn()

idfn(12)
x = idfn(34)
y = idfn(56) + 1
print idfn(78)
print "test: %d" % idfn(90)
print "test:", idfn(12)
z = idfn(1), idfn(2)

lfn()
x = lfn()
y = lfn() or 12
print lfn()
print "test: %s" % lfn()
print "test:", lfn()
z = lfn(), lfn()

k = intfn(), nonefn(), idfn(2), lfn()

if nonefn():
    print "not ok"

if intfn() == 1:
    print "ok"

if len(lfn()):
    print "ok"
