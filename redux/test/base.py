import itertools
import os

import astor

from redux.utils import to_source


class BaseTest(object):

    @staticmethod
    def path(filename):
        return os.path.relpath(os.path.join(os.path.dirname(__file__),
                                            "data", filename), ".")

    @staticmethod
    def expected_path(filename, filter_name):
        expected_filename = filename.replace(".py", "_%s.py" % filter_name)
        return os.path.relpath(
            os.path.join(os.path.dirname(__file__), "data",
            "expected", expected_filename), ".")

    def assertMultiLineEqualStrip(self, str1, str2, message=None,
                                  ignore_lines=None):
        """
        Same as assertMultiLineEqual but strip each line before comparing
        ignore_lines can be a set of lines to ignore (starting from 1)
        """
        lines1 = str1.splitlines()
        lines2 = str2.splitlines()
        errors = []
        for index, (l1, l2) in enumerate(itertools.izip_longest(lines1,
                                                                lines2)):
            if ignore_lines and index + 1 in ignore_lines:
                continue

            line_message = "line %d: %s != %s" % (index + 1, l1, l2)
            if (l1 and l1.strip()) != (l2 and l2.strip()):
                errors.append(line_message)
        print "\n".join(errors)
        if len(errors):
            print str1
        self.assertEqual(len(errors), 0, message)

    def load_source(self, filename):
        return astor.misc.parsefile(self.path(filename))

    def load_expected(self, filename, name):
        return open(self.expected_path(filename, name)).read()

    def verify_result(self, result, filename, name, ignore_lines=None):
        expected_result = self.load_expected(filename, name)

        actual_result = to_source(result)
        self.assertMultiLineEqualStrip(actual_result, expected_result,
                                       filename + name, ignore_lines)
