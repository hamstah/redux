import unittest

import astor

from redux.scanners import FindUnusedAssignments

from base import BaseTest
from redux.utils import node_targets


class FindUnusedAssignmentsTest(BaseTest, unittest.TestCase):

    sources = [
        ("unused1.py", {
            2: ["a"],
            6: ["b"],
            10: ["i"],
            16: ["b"],
            20: ["a", "b"],
            26: ["a", "b"],
            30: ["i", "index"]
        }, {
            40: ["i"],
            50: ["a"],
            55: ["i", "k"],
        })
    ]

    def test_scanner(self):

        for filename, expected, expected_partial in self.sources:
            loaded = astor.misc.parsefile(self.path(filename))
            scanner = FindUnusedAssignments()
            scanner.visit(loaded)

            actual = {}
            for unused in scanner.unused:
                actual[unused.lineno] = sorted([node.id for node
                                                in node_targets(unused)])
            self.assertEqual(actual, expected)

            actual_partial = {}
            for parent, partial_unused in scanner.partial_unused.items():
                actual_partial[parent.lineno] = sorted([node.id for node
                                                        in partial_unused])
            self.assertEqual(actual_partial, expected_partial)
