import copy
import unittest

import astor

from redux.filters import (
    SimplifyRemovePrints,
    SimplifyFormats,
    RemoveAsserts,
    RemoveEmptyIfs,
    RemoveExtraPasses,
    RemoveFunctionCall,
    RemovePrints,
    RemoveUnusedDeclarations,
    RemoveUnusedLines,
    ReplaceFunctionCallByReturnValue,
)

from redux.utils import run

from base import BaseTest


class FilterBaseTest(BaseTest):

    def test_filter(self):

        for filename in self.sources:
            loaded = self.load_source(filename)
            redux_filter = self.filter()
            result = redux_filter.visit(loaded)
            self.assertTrue(redux_filter.modified_ast)

            name = redux_filter.name
            self.verify_result(result, filename, name)

    def filter(self):
        return self.filter_class()


class SimplifyRemovePrintsTest(FilterBaseTest, unittest.TestCase):
    sources = [
        "print1.py"
    ]
    filter_class = SimplifyRemovePrints


class SimplifyFormatsTest(FilterBaseTest, unittest.TestCase):
    sources = [
        "print1.py"
    ]
    filter_class = SimplifyFormats


class RemovePrintsTest(FilterBaseTest, unittest.TestCase):
    sources = [
        "functions1.py",
        "functions3.py",
        "print1.py",
        "unused1.py",
    ]
    filter_class = RemovePrints


class RemoveExtraPassesTest(FilterBaseTest, unittest.TestCase):
    sources = [
        "pass1.py",
        "pass2.py",
        "pass4.py",
    ]
    filter_class = RemoveExtraPasses


class RemoveEmptyIfsTest(FilterBaseTest, unittest.TestCase):
    sources = [
        "pass2.py",
        "pass3.py",
    ]
    filter_class = RemoveEmptyIfs


class ReplaceFunctionCallByReturnValueTest(BaseTest,
                                           unittest.TestCase):
    sources = [
        "functions1.py"
    ]

    def test_filter(self):

        for filename in self.sources:
            current_ast = self.load_source(filename)
            tr, ns, _ = run(self.path(filename), current_ast, None)

            for (tr_filename, line_no, func_name), values in tr.results.items():
                for index, val in enumerate(values):
                    current_ast = ReplaceFunctionCallByReturnValue(
                        tr_filename, line_no, func_name,
                        index, val).visit(current_ast)

            name = "replacefunctioncallbyreturnvalue"
            self.verify_result(current_ast, filename, name)


class RemoveFunctionCallTest(BaseTest, unittest.TestCase):
    sources = [
        "functions1.py"
    ]

    def test_filter(self):

        for filename in self.sources:
            current_ast = self.load_source(filename)
            tr, ns, _ = run(self.path(filename), current_ast, None)

            for (tr_filename, line_no, func_name), values in tr.results.items():
                for index, val in enumerate(values):
                    current_ast = RemoveFunctionCall(
                        tr_filename, line_no, func_name, index).visit(current_ast)

            name = "removefunctioncall"
            self.verify_result(current_ast, filename, name)


class RemoveUnusedDeclarationsTest(BaseTest, unittest.TestCase):

    sources = [
        "functions2.py"
    ]

    def test_filter(self):
        for filename in self.sources:
            current_ast = astor.misc.parsefile(self.path(filename))
            tr, ns, _ = run(self.path(filename), current_ast, None)

        for node_type in [["function"], ["class"], ["class", "function"]]:
            copied = copy.deepcopy(current_ast)
            filter = RemoveUnusedDeclarations(
                run=tr.coverage[self.path(filename)],
                node_type=node_type)
            modified_ast = filter.visit(copied)
            name = filter.name + "_" + "_".join(node_type)  # clean up
            self.verify_result(modified_ast, filename, name)


class RemoveUnusedLinesTest(BaseTest, unittest.TestCase):

    sources = [
        "functions2.py",
        "functions3.py",
    ]

    def test_filter(self):
        for filename in self.sources:
            current_ast = self.load_source(filename)
            tr, ns, _ = run(self.path(filename), current_ast, None)

        filter = RemoveUnusedLines(run=tr.coverage[self.path(filename)])
        modified_ast = filter.visit(current_ast)
        name = filter.name
        self.verify_result(modified_ast, filename, name)
