from exc import SentinelException


def fn(y):
    if y > 45 and y % 7 == 0:
        return None
    return y + 1

for i in xrange(100):
    try:
        x = fn(i) + 10
        print x
    except TypeError:
        raise SentinelException
