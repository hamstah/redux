import collections
import inspect
import sys


class Tracer(object):
    """
    Context manager to keep track of function calls and their return values.
    It computes a signature of the location of where function are called
    and stores the value returned.

    If a function is called multiple times on a single line, each return value
    is stored in a list.
    """

    def __init__(self, filename):
        self.filename = filename
        # store the list of retun values for a given signature
        self.results = collections.defaultdict(lambda: [])
        self.coverage = collections.defaultdict(lambda: set())
        self.files = set()

    def __enter__(self):
        # store the existing tracing if any so we can call it in the
        # hook and not break tools like coverage etc
        self.original = sys.gettrace()
        sys.settrace(self.trace)
        return self

    def __exit__(self, type, value, traceback):
        sys.settrace(self.original)

    def trace(self, frame, event, arg):
        if self.original:
            self.original(frame, event, arg)

        co = frame.f_code
        filename = co.co_filename
        line_no = frame.f_lineno
        func_name = co.co_name

        if event == "line":
            self.coverage[filename].add(line_no)
            return

        # keep track of seen filenames
        if not filename in self.coverage:
            self.coverage[filename] = set()

        if filename != self.filename:
            return

        if func_name.startswith("_"):
            return

        if event == "call":
            # needed to trace inside the called function
            return self.trace

        elif event == "return":
            # here the line_no and filename are the location
            # of the return. We want to get the location of the caller
            # the caller is the parent frame
            # the parent frame is frame.f_back
            parent_frame = frame.f_back
            parent_filename = inspect.getsourcefile(parent_frame)
            parent_line_no = parent_frame.f_lineno
            signature = (parent_filename, parent_line_no, func_name)
            self.results[signature].append(arg)
            return
