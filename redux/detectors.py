import re


class FailureDetector(object):
    pass


class ClassFailureDetector(FailureDetector):

    def __init__(self, klass):
        self.klass = klass

    def __call__(self, e):
        if isinstance(self.klass, basestring):
            return e.__class__.__name__ == self.klass

        return e.__class__ is self.klass


class MessageFailureDetector(FailureDetector):

    def __init__(self, message):
        self.message = re.compile(message)

    def __call__(self, e):
        return self.message.match(str(e))

