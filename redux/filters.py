import ast

from utils import is_basic


class Deleted(ast.Pass):
    pass


class BaseFilter(ast.NodeTransformer):

    white_list = None

    def check(self, node):
        return not self.white_list or node.__class__ in self.white_list

    def visit(self, node):
        if hasattr(node, "_delete") and node._delete:
            self._modified = True
            return None

        # visit children
        for child in ast.iter_child_nodes(node):
            child._parent = node
        self.generic_visit(node)
        if not self.check(node):
            return node

        return self.apply(node)

    def modified_ast(self):
        return hasattr(self, "_modified")

    @property
    def name(self):
        return self.__class__.__name__.lower()


class RemoveUnusedDeclarations(BaseFilter):

    target_classes = {
        "class": ast.ClassDef,
        "function": ast.FunctionDef,
    }
    white_list = [ast.FunctionDef, ast.ClassDef]

    def __init__(self, not_run=None, run=None, node_type=None):
        assert not_run is not None or run is not None
        self.not_run = not_run
        self.run = run
        try:
            self.targets = [self.target_classes[n]
                            for n in node_type]
        except TypeError:
            self.targets = [self.target_classes[node_type]]

    def visit(self, node):
        self.generic_visit(node)

        if node.__class__ not in self.targets:
            return node

        keep_node = False
        body = getattr(node, "body", ast.iter_child_nodes(node))
        for child in body:
            # TODO: check if args have default values with side effects

            # child not called, keep checking nodes
            unused = ((self.not_run and child.lineno in self.not_run) or
                      (self.run and child.lineno not in self.run))

            if (unused or hasattr(child, "_delete") or
                    child.__class__ in [ast.Pass, Deleted]):
                continue

            # the child is called, need to keep the node
            keep_node = True
            break

        # remove the node
        if not keep_node:
            self._modified = True
            return ast.copy_location(Deleted(), node)

        return node


class RemoveUnusedLines(BaseFilter):

    def __init__(self, not_run=None, run=None):
        assert not_run is not None or run is not None
        self.not_run = not_run
        self.run = run

    def apply(self, node):
        line_no = getattr(node, "lineno", None)
        if line_no is None:
            return node

        unused = ((self.not_run and line_no in self.not_run) or
                  (self.run and line_no not in self.run))
        if unused:
            self._modified = True
            return ast.copy_location(Deleted(), node)

        return node


class RemoveExtraPasses(BaseFilter):
    """
    Removes consecutive pass statement, or pass statement followed
    by an actual statement.
    """

    def apply(self, node):
        previous_pass = False
        previous_child = None
        non_passes = 0  # counts the number on statement that aren't ast.Pass

        # if the node has a body, use that instead of the normal list of
        # children to avoid counting arguments for function defs etc
        body = getattr(node, "body", ast.iter_child_nodes(node))
        for child in body:
            if previous_pass:
                previous_child._delete = True

            if child.__class__ in [ast.Pass or Deleted]:
                previous_pass = True
            else:
                non_passes += 1
                previous_pass = False
            previous_child = child

        # handle the case of a block ending with a pass which also contains
        # statements that are not pass.
        if previous_child.__class__ in [ast.Pass, Deleted] and non_passes > 0:
            previous_child._delete = True
        # TODO: remove the child node directly with delattr, see the
        # base generic_visit code
        self.generic_visit(node)
        return node


class RemoveEmptyIfs(BaseFilter):
    """
    Replaces if nodes with empty body by an assert of the
    inverse of the conditional.

    The conditional is kept in case the evaluation has side effects
    """

    white_list = [ast.If]

    def apply(self, node):
        keep_if = False
        for child in node.body:

            if child.__class__ is Deleted:
                continue
            keep_if = True
            break

        if not keep_if:
            self._modified = True
            return ast.copy_location(ast.Assert(
                test=ast.UnaryOp(op=ast.Not(), operand=node.test),
                msg=None,
            ), node)

        return node


class SimplifyFormats(BaseFilter):
    """
    Simplifies strings formatted with the modulo operator.
    Removes the node if the right side
    of the modulo has a class in safe_to_remove
    """

    white_list = [ast.BinOp]
    safe_to_remove = [ast.Num, ast.Str]

    def check(self, node):
        if not super(SimplifyFormats, self).check(node):
            return False
        return node.op.__class__ is ast.Mod and node.left.__class__ is ast.Str

    def apply(self, node):
        if is_basic(node.right):
            self._modified = True
            return None

        return node


class SimplifyRemovePrints(BaseFilter):
    """
    Simplifies print nodes by removing basic types
    from them, and deleting the node if there is nothing left
    """

    white_list = [ast.Print]
    safe_to_remove = [ast.Num, ast.Str]
    simplify_formats = SimplifyFormats()

    def apply(self, node):
        values = []
        for value in node.values:
            if is_basic(value):
                continue
            # try to simplify the format of the child
            # will return None if the child can be deleted
            res = self.simplify_formats.visit(value)
            if not res:
                continue
            values.append(res)

        node.values = values
        keep_print = len(node.values) != 0

        if not keep_print:
            self._modified = True
            return None

        return node


class ModifyFunctionCall(BaseFilter):

    white_list = [ast.Call]

    def __init__(self, filename, line_no, func_name, call_order):
        self.filename = filename
        self.line_no = line_no
        self.func_name = func_name
        self.call_order = call_order

        self.seen = 0

    def check(self, node):
        if not super(ModifyFunctionCall, self).check(node):
            return False

        # check if a node is replacing an old function call
        # this happens if a multiple transformations are applied
        # to functions on the same line without refreshing the call_order
        # in the filter. This makes sure the seen/call_order are in sync
        replaced = getattr(node, "_replaced", None)
        if not replaced and node.__class__ != ast.Call:
            return False

        line_no = getattr(node, "lineno", None)
        if line_no != self.line_no:
            return False

        if not replaced and node.func.__class__ is not ast.Name:
            return False

        if not replaced:
            func_name = node.func.id
        else:
            func_name = replaced

        if func_name != self.func_name:
            return False

        self.seen += 1
        return self.seen == self.call_order + 1


class ReplaceFunctionCallByReturnValue(ModifyFunctionCall):

    white_list = [ast.Call, ast.Name, ast.Num, ast.Str]

    def __init__(self, filename, line_no, func_name, call_order, return_value):
        super(ReplaceFunctionCallByReturnValue, self).__init__(filename, line_no, func_name, call_order)
        self.return_value = return_value

    def apply(self, node):
        t = type(self.return_value)
        type_map = {
            int: ast.Num,
            float: ast.Num,
            str: ast.Str,
        }
        ret_type = type_map.get(t, None)
        if ret_type:
            result = ret_type(self.return_value)
        elif t is bool:
            result = ast.Name(id='%s' % self.return_value, ctx=ast.Load())
        elif self.return_value is None:
            result = ast.Name(id='None', ctx=ast.Load())
        else:
            return node

        self._modified = True
        result._replaced = self.func_name
        return ast.copy_location(result, node)


class RemoveFunctionCall(ModifyFunctionCall):

    white_list = [ast.Call, ast.Name, ast.Num, ast.Str]

    keep_if_parent = [ast.Expr, ast.Assign, ast.Assert, ast.BinOp,
                      ast.IfExp, ast.Tuple, ast.If, ast.Compare, ast.Call]

    def apply(self, node):
        self._modified = True

        # for some parents, we need to keep a node to not break them
        if node._parent.__class__ in self.keep_if_parent:
            res = ast.copy_location(ast.Num(0), node)
            res._replaced = self.func_name
            return res

        return None


class RemoveNode(BaseFilter):

    def apply(self, node):
        self._modified = True

        body = getattr(node._parent, "body", ast.iter_child_nodes(node._parent))
        for child in body:
            if child != node:
                # found another node in the parent, so we can
                # safely delete the node
                return None

        # node was the only child of the parent, replace by pass
        return ast.copy_location(Deleted(), node)


class RemoveSpecificNodes(RemoveNode):

    def __init__(self, targets):
        self.targets = []
        try:
            for target in targets:
                self.targets.append(target)
        except TypeError:
            self.targets.append(targets)

    def compare(self, node1, node2):
        if node1.__class__ != node2.__class__:
            return False

        # TODO add check for all fields with ast.iter_fields
        id1 = getattr(node1, "id", None)
        id2 = getattr(node2, "id", None)
        if id1 != id2:
            return False

        children1 = ast.iter_child_nodes(node1)
        children2 = ast.iter_child_nodes(node2)

        for c1, c2 in zip(children1, children2):
            res = self.compare(c1, c2)
            if not res:
                return False

        return True

    def check(self, node):
        res = super(RemoveSpecificNodes, self).check(node)
        if not res:
            return False

        for target in self.targets:
            if self.compare(target, node):
                return True
        return False


class RemovePrints(RemoveNode):
    white_list = [ast.Print]


class RemoveAsserts(RemoveNode):
    white_list = [ast.Assert]


class RemoveDeleted(RemoveNode):
    white_list = [Deleted]

# can't make that one work with remove unused lines
# maybe messing with lineno?
#class SplitPrints(BaseFilter):
#
#    white_list = [ast.Print]
#
#    def apply(self, node):
#        self._modified = True
#
#        if len(node.values) < 1:
#            return None
#
#        print "split", node.values
#        #return [ast.Expr(v) for v in node.values]
#        return ast.Expr(value=ast.Tuple(elts=node.values, ctx=ast.Load()))
#
