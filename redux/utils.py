import ast
import contextlib
import os
import StringIO
import sys


from astor.codegen import SourceGenerator as AstorSourceGenerator

from .exc import SentinelException, CantFindTargetFunctionError
from .tracer import Tracer

# Inspired By Jochen Ritzel's answer
# from http://stackoverflow.com/a/3906390/338325
@contextlib.contextmanager
def capture_output():
    old_out = sys.stdout

    sys.stdout = StringIO.StringIO()

    yield
    sys.stdout = old_out


def is_name(ast_node, id_str):
    if ast_node.__class__ is not ast.Name:
        return False
    return ast_node.id == id_str


def is_bool(ast_node):
    return is_name(ast_node, "True") or is_name(ast_node, "False")


def is_none(ast_node):
    return is_name(ast_node, "None")


def is_basic(ast_node):
    return (ast_node.__class__ in [ast.Num, ast.Str] or
            is_none(ast_node) or
            is_bool(ast_node))


def node_targets(node):
    if node.__class__ is ast.Name:
        return [node]

    if node.__class__ is ast.Assign:
        return sum([node_targets(node) for node in node.targets], [])

    if node.__class__ is ast.For:
        return node_targets(node.target)

    if node.__class__ is ast.Tuple:
        return sum([node_targets(elt) for elt in node.elts], [])

    return []


def load_source(filename):
    working_dir = os.path.dirname(filename)
    source = open(filename).read()
    return "\n".join(["import sys",
                      "sys.path.insert(0, '%s')" % working_dir,
                      source])


def run(filename, target_ast=None, function_name=None, error_detector=None):
    if not target_ast:
        target = load_source(filename)
    else:
        target = ast.fix_missing_locations(target_ast)

    raised = False
    ns = {}
    try:
        code = compile(target, filename, 'exec')
    except:
        raise
    with capture_output():
        with Tracer(filename) as tr:
            try:
                exec code in ns
                if function_name:
                    if function_name not in ns:
                        raise CantFindTargetFunctionError(function_name,
                                                          filename,
                                                          target_ast)
                    ns[function_name]()
            except SentinelException:
                raised = True
            except CantFindTargetFunctionError:
                raise
            except Exception as e:
                if error_detector and error_detector(e):
                    raised = True
                else: raise
                pass

    return tr, ns, raised


class SourceGenerator(AstorSourceGenerator):

    def visit_Deleted(self, node):
        return self.visit_Pass(node)


def to_source(node, indent_with=' ' * 4, add_line_information=False):
    generator = SourceGenerator(indent_with, add_line_information)
    generator.visit(node)
    return ''.join(str(s) for s in generator.result)

