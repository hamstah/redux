class SentinelException(Exception):
    pass


class CodeException(Exception):

    def __init__(self, filename, code=None):
        self.filename = filename
        self.code = code
        super(CodeException, self).__init__(self.message)


class UnexpectedSuccessError(CodeException):
    message = "The code didn't fail as expected"
    advice = ("Could not verify the failure, make sure the code raises "
              "SentinelException or uses the right failure detector")


class CantFindTargetFunctionError(CodeException):
    advice = "Check the function exists and try again"

    def __init__(self, function_name, filename, code=None):
        self.function_name = function_name
        super(CantFindTargetFunctionError, self).__init__(
            filename, code
        )

    @property
    def message(self):
        return "Could not find '%s' in '%s'" % (self.function_name,
                                                self.filename)
